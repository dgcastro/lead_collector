package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
//@Table(name ="nomeDaTabela")
public class Lead {
 //   @Column(name = 'nomeCompleto')

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;
    private String email;
    private LocalDate data;
    private TipoLeadEnum tipoLead;

    public Lead() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }
}
