package br.com.lead.collector.controllers;


import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public Produto registrarProduto(@RequestBody Produto produto){
        return produtoService.registrarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> exibirProdutos(){
        return produtoService.buscarProdutos();
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") long id, @RequestBody Produto produto ){
        try{
            Produto produtoObjeto = produtoService.atualizarProduto(id, produto);
            return produtoObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable long id){
        try{
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }
}
