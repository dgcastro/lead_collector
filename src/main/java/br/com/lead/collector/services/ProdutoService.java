package br.com.lead.collector.services;


import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto registrarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarProdutos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto atualizarProduto(long id, Produto produto){
        if(produtoRepository.existsById(id)){
            produto.setId(id);
            Produto produtoObjeto = registrarProduto(produto);
            return produtoObjeto;
        }else{
            throw new RuntimeException("O Produto não foi encontrado.");
        }
    }

    public void deletarProduto(long id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        }else{
            throw new RuntimeException("O Produto não foi encontrado.");
        }
    }
}
